#include "lexer.h"

int GetCurrentTokenPrecedence(Lexer* lexer) {
    // Make sure it's a declared binop.
    int precedence = lexer->_binopPrecedence[lexer->_currentToken];
    if (precedence <= 0) return -1;
    return precedence;
}

/// IterateToken - Return the next token from standard input.
void IterateToken(Lexer* lexer) {
    LogMessage("IterateToken()");

    // Skip any whitespace.
    while (isspace(lexer->_lastChar)) {
        lexer->_lastChar = getchar();
    }

    if (isalpha(lexer->_lastChar)) { // identifier: [a-zA-Z][a-zA-Z0-9]*
        lexer->_identifier = malloc(1);
        lexer->_identifier[0] = '\0';

        AppendChar(&lexer->_identifier, lexer->_lastChar);
        while (isalnum((lexer->_lastChar = getchar()))) {
            AppendChar(&lexer->_identifier, lexer->_lastChar);
        }

        if (strcmp(lexer->_identifier, "def") == 0) {
            lexer->_currentToken = TOKEN_TYPE;
            LogMessage(lexer->_identifier);
            return;
        }

        if (strcmp(lexer->_identifier, "extern") == 0) {
            lexer->_currentToken = TOKEN_EXTERN;
            LogMessage(lexer->_identifier);
            return;
        }

        lexer->_currentToken = TOKEN_IDENTIFIER;
        LogMessage(lexer->_identifier);
        return;
    }

    if (isdigit(lexer->_lastChar) || lexer->_lastChar == '.') { // Number: [0-9.]+
        char* numberString = malloc(sizeof(char));
        numberString[0] = NULL;
        do {
            AppendChar(&numberString, lexer->_lastChar);
            lexer->_lastChar = getchar();
        }
        while (isdigit(lexer->_lastChar) || lexer->_lastChar == '.');

        lexer->_number = strtod(numberString, NULL);
        free(numberString);
        lexer->_currentToken = TOKEN_NUMBER;
        LogMessage(numberString);
        return;
    }

    if (lexer->_lastChar == '#') {
        // Comment until end of line.
        do {
            lexer->_lastChar = getchar();
        }
        while (lexer->_lastChar != EOF &&lexer->_lastChar != '\n' &&  lexer->_lastChar != '\r');

        if (lexer->_lastChar != EOF) {
            IterateToken(lexer);
        }
    }

    // Check for end of file.  Don't eat the EOF.
    switch (lexer->_lastChar)
    {
        case EOF:
            lexer->_currentToken = TOKEN_EOF; 
            return;
        case '(':
            lexer->_currentToken = TOKEN_OPEN_PAREN; break;
        case ')':
            lexer->_currentToken = TOKEN_CLOSE_PAREN; break;
        case ',':
            lexer->_currentToken = TOKEN_COMMA; break;
        case ';':
            lexer->_currentToken = TOKEN_SEMICOLON; break;
        case '+':
            lexer->_currentToken = TOKEN_ADD; break;
        case '-':
            //_currentToken = TOKEN_SUB; break;
        case '*':
            //_currentToken = TOKEN_MUL; break;
        case '/':
            //_currentToken = TOKEN_DIV; break;
        case '<':
            //_currentToken = TOKEN_LESS_THAN; break;
        case '>':
            //_currentToken = TOKEN_GREATER_THAN; break;
        case ' ':
            //_currentToken = TOKEN_SPACE; break; //this probably isnt needed.
            break;
        default:
            LogErrorChar("unknown char", lexer->_lastChar);
            break;
    }

    // Otherwise, just return the character as its ascii value.
    // IterateToken is expected to consume a char, so it is required
    // to getchar in all paths atleast once.
    lexer->_lastChar = getchar();
}
