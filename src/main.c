#include "parser.h"

int main()
{
	verbosity = ERROR;

    Lexer* lexer = malloc(sizeof(Lexer));
    lexer->_lastChar = ' ';
    lexer->_currentToken = TOKEN_UNKNOWN;
    lexer->_identifier = NULL; // Filled in if tok_identifier
    lexer->_number = 0.0; // Filled in if tok_number

    // Install standard binary operators.
    // 1 is lowest precedence.
    lexer->_binopPrecedence[TOKEN_LESS_THAN] = 10;
    lexer->_binopPrecedence[TOKEN_ADD] = 20;
    lexer->_binopPrecedence[TOKEN_SUB] = 20;
    lexer->_binopPrecedence[TOKEN_MUL] = 40;  // highest.

    Parser* parser = malloc(sizeof(Parser));
    parser->_lexer = lexer;

    // Prime the first token.
    fprintf(stderr, "ready> ");
    IterateToken(parser->_lexer);

    MainLoop(parser);

    free(lexer);
    free(parser); 
    return 0;
}