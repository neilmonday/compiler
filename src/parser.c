#include "parser.h"

/// numberexpr ::= number
ExprAST* ParseNumberExpr(Lexer* lexer) {
    LogMessage("ParseNumberExpr()");
    IterateToken(lexer); // consume the number
    ExprAST* expr = malloc(sizeof(ExprAST));
    expr->_type = NUMBER_EXPRESSION;
    expr->_number = lexer->_number;
    return(expr);
}

/// parenexpr ::= '(' expression ')'
ExprAST* ParseParenExpr(Lexer* lexer) {
    LogMessage("ParseParenExpr()");
    IterateToken(lexer); // eat (.
    ExprAST* V = ParseExpression(lexer);
    if (!V)
    {
        return NULL;
    }

    if (lexer->_currentToken != TOKEN_CLOSE_PAREN)
    {
        LogError("expected ')'");
        return NULL;
    }
    IterateToken(lexer); // eat ).
    return V;
}

/// identifierexpr
///   ::= identifier
///   ::= identifier '(' expression* ')'
ExprAST* ParseIdentifierExpr(Lexer* lexer) {
    LogMessage("ParseIdentifierExpr()");
    char* name = lexer->_identifier;

    IterateToken(lexer);  // eat identifier.

    if (lexer->_currentToken != TOKEN_OPEN_PAREN) {
        ExprAST* expr = malloc(sizeof(ExprAST));
        expr->_type = IDENTIFIER_EXPRESSION;
        expr->_name = lexer->_identifier;
        return expr;
    }
        
    // Call.
    IterateToken(lexer);  // eat (

    //Build the args list
    ExprAST** args = malloc(sizeof(ExprAST*));
    args[0] = malloc(sizeof(ExprAST));
    args[0] = NULL;
    if (lexer->_currentToken != TOKEN_CLOSE_PAREN) {
        while (1) {
            ExprAST* arg = ParseExpression(lexer);
            if (arg != NULL)
            {
                AppendExprAST(&args, arg);
            }
            else
            {
                return NULL;
            }

            if (lexer->_currentToken == TOKEN_CLOSE_PAREN)
            {
                break;
            }

            if (lexer->_currentToken != TOKEN_COMMA)
            {
                LogError("Expected ')' or ',' in argument list");
                return NULL;
            }

            IterateToken(lexer);
        }
    }

    // Eat the ')'.
    IterateToken(lexer);
    ExprAST* expr = malloc(sizeof(ExprAST));
    expr->_type = CALL_EXPRESSION;
    expr->_args = args;
    expr->_name = name;
    return expr;
}

/// primary
///     ::= identifierexpr
///     ::= numberexpr
///     ::= parenexpr
ExprAST* ParsePrimary(Lexer* lexer) {
    LogMessage("ParsePrimary()");
    switch (lexer->_currentToken) {
    default:
        LogErrorInt("unknown token when expecting an expression", lexer->_currentToken);
        return NULL;
    case TOKEN_IDENTIFIER:
        return ParseIdentifierExpr(lexer);
    case TOKEN_NUMBER:
        return ParseNumberExpr(lexer);
    case TOKEN_OPEN_PAREN:
        return ParseParenExpr(lexer);
    }
}

/// expression
///     ::= primary binoprhs
///
ExprAST* ParseExpression(Lexer* lexer) {
    LogMessage("ParseExpression()");
    ExprAST* LHS = ParsePrimary(lexer);
    if (!LHS)
        return NULL;

    return ParseBinOpRHS(lexer, 0, LHS);
}

/// binoprhs
///   ::= ('+' primary)*
ExprAST* ParseBinOpRHS(Lexer* lexer, int expressionPrecedence, ExprAST* LHS) {
    LogMessage("ParseBinOpRHS()");
    // If this is a binop, find its precedence.
    while (1) {
        int tokenPrecedence = GetCurrentTokenPrecedence(lexer);

        // If this is a binop that binds at least as tightly as the current binop,
        // consume it, otherwise we are done.
        if (tokenPrecedence < expressionPrecedence) {
            return LHS;
        }
            
        // Okay, we know this is a binop.
        int BinOp = lexer->_currentToken;
        IterateToken(lexer); // eat binop

        // Parse the primary expression after the binary operator.
        ExprAST* RHS = ParsePrimary(lexer);
        if (!RHS) {
            return NULL;
        }

        // If BinOp binds less tightly with RHS than the operator after RHS, let
        // the pending operator take RHS as its LHS.
        int nextPrecedence = GetCurrentTokenPrecedence(lexer);
        if (tokenPrecedence < nextPrecedence) {
            RHS = ParseBinOpRHS(lexer, tokenPrecedence + 1, RHS);
            if (!RHS) {
                return NULL;
            }
        }

        // Merge LHS/RHS.
        ExprAST* expr = malloc(sizeof(ExprAST));
        expr->_type = BINARY_EXPRESSION;
        expr->_left = LHS;
        expr->_right = RHS;
        expr->_op = BinOp;
        return expr;
    }
}

/// prototype
///   ::= id '(' id* ')'
PrototypeAST* ParsePrototype(Lexer* lexer) {
    LogMessage("ParsePrototype()");
    if (lexer->_currentToken != TOKEN_IDENTIFIER) {
        LogError("Expected function name in prototype");
        return NULL;
    }

    char* functionName = lexer->_identifier;
    IterateToken(lexer);

    if (lexer->_currentToken != TOKEN_OPEN_PAREN) {
        LogError("Expected '(' in prototype");
        return NULL;
    }

    char** args = malloc(sizeof(char*));
    args[0] = malloc(sizeof(char));
    args[0] = NULL;
    while (1) {
        IterateToken(lexer);
        if (lexer->_currentToken == TOKEN_IDENTIFIER) {
            AppendString(&args, lexer->_identifier);
        } else {
            break;
        }
    }

    if (lexer->_currentToken != TOKEN_CLOSE_PAREN) {
        LogError("Expected ')' in prototype");
        return NULL;
    }

    // success.
    IterateToken(lexer);  // eat ')'.
    PrototypeAST* prototype = malloc(sizeof(PrototypeAST));
    prototype->_name = functionName;
    prototype->_args = args;
    return prototype;
}

/// definition ::= 'def' prototype expression
FunctionAST* ParseDefinition(Lexer* lexer) {
    LogMessage("ParseDefinition()");
    IterateToken(lexer);  // eat def.
    PrototypeAST* prototype = ParsePrototype(lexer);
    if (!prototype)
    {
        return NULL;
    }

    ExprAST* expression = ParseExpression(lexer);
    if (expression != NULL) {
        FunctionAST* function = malloc(sizeof(FunctionAST));
        function->_proto = prototype;
        function->_body = expression;
        return function;
    }
        
    return NULL;
}

/// external ::= 'extern' prototype
PrototypeAST* ParseExtern(Lexer* lexer) {
    LogMessage("ParseExtern()");
    IterateToken(lexer);  // eat extern.
    return ParsePrototype(lexer);
}

/// toplevelexpr ::= expression
FunctionAST* ParseTopLevelExpr(Lexer* lexer) {
    LogMessage("ParseTopLevelExpr()");
    ExprAST* expression = ParseExpression(lexer);
    if (expression) {
        // Make an anonymous proto.
        PrototypeAST* prototype = malloc(sizeof(PrototypeAST));
        //PrototypeAST* prototype = new PrototypeAST(lexer, "", std::vector<std::string>());
        //char*  _name;
        //char* _args; //new char*[somesize] an array of cstrings
        FunctionAST* function = malloc(sizeof(FunctionAST));
        function->_proto = prototype;
        function->_body = expression;
        return function;
    }
    return NULL;
}

void HandleDefinition(Lexer* lexer) {
    LogMessage("HandleDefinition()");
    if (ParseDefinition(lexer)) {
        LogError("Parsed a function definition.");
    } else {
        // Skip token for error recovery.
        IterateToken(lexer);
    }
}

void HandleExtern(Lexer* lexer) {
    LogMessage("HandleExtern()");
    if (ParseExtern(lexer)) {
        LogError("Parsed an extern");
    } else {
        // Skip token for error recovery.
        IterateToken(lexer);
    }
}

void HandleTopLevelExpression(Lexer* lexer) {
    LogMessage("HandleTopLevelExpression()");
    // Evaluate a top-level expression into an anonymous function.
    if (ParseTopLevelExpr(lexer)) {
        LogError("Parsed a top-level expr");
    } else {
        // Skip token for error recovery.
        IterateToken(lexer);
    }
}

/// top ::= definition | external | expression | ';'
void MainLoop(Parser* parser) {
    LogMessage("MainLoop()");

    while (1) {
        fprintf(stderr, "ready> ");
        switch (parser->_lexer->_currentToken) {
            case TOKEN_EOF:
                return;
            case TOKEN_SEMICOLON: //end of the line.
                IterateToken(parser->_lexer);
                break;
            case TOKEN_TYPE:
                HandleDefinition(parser->_lexer);
                break;
            case TOKEN_EXTERN:
                HandleExtern(parser->_lexer);
                break;
            default:
                HandleTopLevelExpression(parser->_lexer);
                break;
        }
    }
}