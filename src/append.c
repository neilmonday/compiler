#include "append.h"

//this calls malloc too many times.
void AppendChar(char** str, char c)
{
    size_t len = strlen(*str);
    char *str2 = malloc(len + 1 + 1); /* one for extra char, one for trailing zero */
    strcpy(str2, *str);
    free(*str); //throw away the old one.
    str2[len] = c;
    str2[len + 1] = '\0';
    *str = str2; //return the new one
}

//this calls malloc too many times.
void AppendString(char*** string_pointers, char* new_string)
{
    int array_length = 0;
    int i = 0;

    //make a copy of the new_string for our array
    size_t new_string_length = strlen(new_string);
    char* new_string_copy = malloc(new_string_length * sizeof(char));
    strcpy(new_string_copy, new_string);
    free(new_string);

    while ((*string_pointers)[array_length++] != NULL)
    {
        //do nothing? len already increments.
    }
    assert(array_length > 0);

    *string_pointers = realloc(*string_pointers, array_length + 1 * sizeof(char*));
    (*string_pointers)[array_length - 1] = new_string_copy;
    (*string_pointers)[array_length] = NULL;
}

//this calls malloc too many times.
void AppendExprAST(ExprAST*** expression_pointers, ExprAST* new_expression)
{
    int array_length = 0;
    int i = 0;

    //make a copy of the new_string for our array
    ExprAST* new_expression_copy = malloc(sizeof(ExprAST));
    *new_expression_copy = *new_expression;  //deep copy
    free(new_expression);

    while ((*expression_pointers)[array_length++] != NULL)
    {
        //do nothing? len already increments.
    }
    assert(array_length > 0);

    *expression_pointers = realloc(*expression_pointers, array_length + 1 * sizeof(ExprAST*));
    (*expression_pointers)[array_length - 1] = new_expression_copy;
    (*expression_pointers)[array_length] = NULL;
}