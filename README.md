A Compiler For LLVM

Builds with GCC on Ubuntu 14.04.04 LTS x64. I build the project with the following:

g++ src/lexer.cpp src/parser.cpp src/main.cpp -I include -ggdb -o compiler

http://llvm.org/releases/3.8.1/docs/tutorial/
