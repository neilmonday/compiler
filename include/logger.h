#ifndef LOGGER_H
#define LOGGER_H

#include <stdio.h>

typedef enum {
    VERBOSE,
    WARNING,
    ERROR
} Verbosity;

//initializing this to a value makes this a "strong" global variable.
//uninitialized global variables are considered weak.
//http://stackoverflow.com/questions/16039004/what-is-the-visibility-scope-of-a-global-variable-in-c#27789275
static Verbosity verbosity = ERROR; 

static void LogError(const char *Str) 
{ 
    fprintf(stderr, "LogError: %s\n", Str); 
}
static void LogErrorInt(const char *Str, int number) 
{ 
    fprintf(stderr, "LogError: %s, %d\n", Str, number); 
}
static void LogErrorChar(const char *Str, char character) 
{ 
    fprintf(stderr, "LogError: %s: %c\n", Str, character); 
}
static void LogMessage(const char *Str) 
{ 
    if(verbosity == VERBOSE)fprintf(stderr, "LogMessage: %s\n", Str); 
}
static void LogMessageInt(const char *Str, int number) 
{ 
    if(verbosity == VERBOSE)fprintf(stderr, "LogMessage: %s, %d\n", Str, number); 
}
static void LogMessageChar(const char *Str, char character) 
{ 
    if(verbosity == VERBOSE)fprintf(stderr, "LogMessage: %s: %c\n", Str, character); 
}

#endif //LOGGER_H