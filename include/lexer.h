#ifndef LEXER_H
#define LEXER_H

//removing stdlib.h will cause an exception on malloc
#include <stdlib.h> //free, malloc, etc
#include <stdio.h>  //printf getchar
#include <string.h> //strlen, strcpy
#include <ctype.h>  //isspace, isalpha, etc.
#include <assert.h>

#include "logger.h"

// The lexer returns tokens [0-255] if it is an unknown character, otherwise one
// of these for known things.
typedef enum {
    // error
    TOKEN_UNKNOWN,// = -1,
    TOKEN_EOF,// = -2,

    // commands
    TOKEN_TYPE,// = -3,
    TOKEN_EXTERN,// = -4,

    // primary
    TOKEN_IDENTIFIER,// = -5,
    TOKEN_NUMBER,// = -6,

    // syntax
    TOKEN_OPEN_PAREN,// = -7,
    TOKEN_CLOSE_PAREN,// = -8,
    TOKEN_COMMA,// = -9,
    TOKEN_COMMENT,// = -10,
    TOKEN_SEMICOLON,// = -11,
    TOKEN_SPACE,// = -12,

    // operators
    TOKEN_ADD,// = -13,
    TOKEN_SUB,// = -14,
    TOKEN_MUL,// = -15,
    TOKEN_DIV,// = -16,

    TOKEN_LESS_THAN,// = -17,
    TOKEN_GREATER_THAN,// = -18,

    TOKEN_SIZE //the number of tokens

} Token;

typedef struct {
    int _lastChar;
    Token _currentToken;
    int _binopPrecedence[TOKEN_SIZE];
    char* _identifier; // Filled in if tok_identifier
    double _number; // Filled in if tok_number
} Lexer;

void IterateToken(Lexer* lexer);
int GetCurrentTokenPrecedence(Lexer* lexer);

#endif //LEXER_H