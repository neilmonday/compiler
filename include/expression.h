#ifndef EXPRESSION_H
#define EXPRESSION_H

typedef enum {
    CALL_EXPRESSION,
    BINARY_EXPRESSION,
    IDENTIFIER_EXPRESSION,
    NUMBER_EXPRESSION
} ExpressionType;

typedef struct ExprAST ExprAST;

/// ExprAST - Base class for all expression nodes.
struct ExprAST {
    ExpressionType _type;
//these should probably be a union based on _type
//struct CallExprAST : public ExprAST {
    char* _callee;
    ExprAST* _args; //new ExprAST[somesize]

//struct BinaryExprAST : public ExprAST {
    char _op;
    ExprAST *_left, *_right;

//struct VariableExprAST : public ExprAST { IDENTIFIER_EXPRESSION
    char* _name;

//struct NumberExprAST : public ExprAST {
    double _number;
};

/// PrototypeAST - This class represents the "prototype" for a function,
/// which captures its name, and its argument names (thus implicitly the number
/// of arguments the function takes).
typedef struct {
    char*  _name;
    char** _args; //new char*[somesize] an array of cstrings
} PrototypeAST;

/// FunctionAST - This class represents a function definition itself.
typedef struct {
    PrototypeAST* _proto;
    ExprAST* _body;
} FunctionAST;

#endif //EXPRESSION_H