#ifndef APPEND_H
#define APPEND_H

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "expression.h"

void AppendChar(char** str, char c);
void AppendString(char*** string_pointers, char* new_string);
void AppendExprAST(ExprAST*** expressions, ExprAST* new_expression);

#endif //APPEND_H