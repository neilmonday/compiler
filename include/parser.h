#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "append.h"
#include "logger.h"
#include "expression.h"
#include "lexer.h"

typedef struct {
    Lexer *_lexer;
} Parser;

ExprAST* ParsePrimary(Lexer* lexer);
ExprAST* ParseExpression(Lexer* lexer);
ExprAST* ParseNumberExpr(Lexer* lexer);
ExprAST* ParseParenExpr(Lexer* lexer);
ExprAST* ParseIdentifierExpr(Lexer* lexer);
ExprAST* ParseBinOpRHS(Lexer* lexer, int expressionPrecedence, ExprAST* LHS);
PrototypeAST* ParsePrototype(Lexer* lexer);
FunctionAST* ParseDefinition(Lexer* lexer);
PrototypeAST* ParseExtern(Lexer* lexer);
FunctionAST* ParseTopLevelExpr(Lexer* lexer);

void HandleDefinition(Lexer* lexer);
void HandleExtern(Lexer* lexer);
void HandleTopLevelExpression(Lexer* lexer);

void MainLoop(Parser* parser);

#endif //PARSER_H